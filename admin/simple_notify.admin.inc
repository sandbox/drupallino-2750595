<?php
/**
 * @file
 * The administration file.
 */

/**
 * The settings-form.
 *
 * @return mixed
 *    Return the drupal form.
 */
function simple_notify_settings() {

  $form = array();
  $token_types = array();

  $form['generalform'] = array(
    '#type' => 'vertical_tabs',
  );

  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
    '#collapsible' => TRUE,
    '#group' => 'generalform',
  );

  $form['general']['simple_notify_on_register'] = array(
    '#type' => 'checkbox',
    '#title' => t('Subscribe all own content on registration'),
    '#default_value' => variable_get('simple_notify_on_register', ''),
    '#option' => array(
      1 => t('Subscribe on register'),
    ),
    '#description' => t('Let user subscribe own created content on registration.'),
    '#group' => 'generalform',
  );

  $form['general']['simple_notify_block_switch'] = array(
    '#type' => 'radios',
    '#title' => t('Notifier Form switch'),
    '#default_value' => variable_get('simple_notify_block_switch', ''),
    '#options' => array(0 => t('Two radios'), 1 => t('One checkbox')),
    '#description' => t('Switch appearance of Notifier form.'),
    '#group' => 'generalform',
  );

  $form['general']['simple_notify_block_template_switch'] = array(
    '#type' => 'radios',
    '#title' => t('Notifier Template/Block switch'),
    '#default_value' => variable_get('simple_notify_block_template_switch', ''),
    '#options' => array(
      0 => t('Use Button as Block'),
      1 => t('Use Button on template'),
    ),
    '#description' => t('Switch how to use the Subscriber-Button. You can use it as
    Drupal-Block and place it where you want or you or configure over the node
    template by using the Tab "Notifier-Button'),
    '#group' => 'generalform',
  );

  $form['mail'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mail'),
    '#collapsible' => TRUE,
    '#group' => 'generalform',
  );

  $form['mail']['simple_notify_mail_sender'] = array(
    '#type' => 'textfield',
    '#title' => t('Mail sender'),
    '#default_value' => variable_get('simple_notify_mail_sender', ''),
    '#token_insert' => TRUE,
    '#description' => t('Mail From'),
    '#group' => 'generalform',
  );

  $form['mail']['simple_notify_mail_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Mail Subject'),
    '#default_value' => variable_get('simple_notify_mail_subject', ''),
    '#token_insert' => TRUE,
    '#description' => t('Switch appearance of Notifier form.'),
    '#group' => 'generalform',
  );

  $form['mail']['simple_notify_mail_body_head'] = array(
    '#type' => 'textarea',
    '#title' => t('Mail Body-Head'),
    '#default_value' => variable_get('simple_notify_mail_body_head', ''),
    '#token_insert' => TRUE,
    '#description' => t('Configure the header of the mail.'),
    '#group' => 'generalform',
  );

  $form['mail']['simple_notify_mail_loop_comments'] = array(
    '#type' => 'textarea',
    '#title' => t('Comment Loop'),
    '#default_value' => variable_get('simple_notify_mail_loop_comments', ''),
    '#token_insert' => TRUE,
    '#description' => t('Here you configure the loop within the body for comments. This means this part will repeat if more than one notification must send'),
    '#group' => 'generalform',
  );

  $form['mail']['simple_notify_mail_loop_references'] = array(
    '#type' => 'textarea',
    '#title' => t('Reference Loop'),
    '#default_value' => variable_get('simple_notify_mail_loop_references', ''),
    '#token_insert' => TRUE,
    '#description' => t('Here you configure the loop within the body references. This means this part will repeat if more than one notification must send'),
    '#group' => 'generalform',
  );

  $form['mail']['simple_notify_mail_body_foot'] = array(
    '#type' => 'textarea',
    '#title' => t('Mail Body-Foot'),
    '#default_value' => variable_get('simple_notify_mail_body_foot', ''),
    '#token_insert' => TRUE,
    '#description' => t('Configure the Footer of the mail.'),
    '#group' => 'generalform',
  );
  // Token tree display STARTS.
  if (module_exists('token')) {
    $form['mail']['token_tree'] = array(
      '#theme' => 'token_tree',
      '#token_types' => $token_types,
      '#group' => 'generalform',
    );
  }
  else {
    $form['mail']['token_tree'] = array(
      '#markup' => '<p>' . t('Enable the <a href="@drupal-token">Token module</a> to view the available token browser.', array('@drupal-token' => 'http://drupal.org/project/token')) . '</p>',
      '#group' => 'generalform',
    );
  }
  // Token tree display ENDS.
  $form['notifier_button'] = array(
    '#type' => 'fieldset',
    '#title' => t('Notifier Button'),
    '#description' => t('Select the content types that are allowed to subscribe to. It make the template variable  <strong>$notifier_button</strong> availeble'),
    '#collapsible' => TRUE,
    '#group' => 'generalform',
  );

  $use_template = variable_get('simple_notify_block_template_switch', '');
  if ($use_template == 1) {
    $content_types = node_type_get_types();
    foreach ($content_types as $option) {
      if (isset($option)) {
        $options[$option->type] = $option->name;
      }
    }
    if (isset($options)) {
      $form['notifier_button']['simple_notify_content_types'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Contenttype'),
        '#default_value' => variable_get('simple_notify_content_types', ''),
        '#options' => $options,
        '#group' => 'generalform',
      );
    }
  }
  else {
    $form['notifier_button']['simple_notify_content_types'] = array(
      '#type' => 'markup',
      '#markup' => 'jfnfj',
      '#group' => 'generalform',
    );
  }

  $form['#submit'][] = 'simple_notify_additional_settings_form_submit';
  return system_settings_form($form);

}

/**
 * Custom settings form submit.
 *
 * @param array $form
 *    The form array.
 * @param array $form_state
 *    The form_state.
 */
function simple_notify_additional_settings_form_submit($form, &$form_state) {
  $content_types = $form_state['values']['content_types'];

  foreach ($content_types as $key => $value) {
    if ($key === $value) {
      $data = field_info_instances("node", $value);
      $variable_data = array();
      foreach ($data as $field) {
        $type = field_info_field($field['field_name']);
        $variable_data[$type['type']][] = $type['field_name'];
      }
      variable_set('simple_notify_' . $key, $variable_data);
    }
  }
}
