<?php
/**
 * @file
 * Readme.md for simple notify module.
 */
?>
#Installation
Enable module on /admin/modules

#Configuration
Configure Simple Notifier on /admin/config/simple-notifier

You can set how Simple Notifier should work. SN can show the 
Notifier-Button by block or put

<code>
    <?php if ($notifier_button) :
?>
            <div class="notifier_button">
                <?php print $notifier_button; ?>
            </div>
        <?php
    endif; ?>
</code>   

in your node.tpl.php (if you have custom templates eg. by 
content-type, put it everywhere) and configure under
/admin/config/simple-notifier section "Notifier Button" on 
which content-type the button should appear.

Configure your mail-settings by using several tokens.
