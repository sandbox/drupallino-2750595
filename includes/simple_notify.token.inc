<?php

/**
 * @file
 * Custom tokens fir simple notifier.
 *
 * Availeble tokens:
 */

/**
 * Implements hook_token_info().
 */
function simple_notify_token_info() {
  $types['simple_notify'] = array(
    'name' => t("Simple Notifier Tokens"),
    'description' => t("Tokens for Simple Notifier."),
  );

  $simple_notify['author-name'] = array(
    'name' => t("Author Name"),
    'description' => t("The username who created the content."),
  );
  $simple_notify['user-id'] = array(
    'name' => t("User ID"),
    'description' => t("The user id who created the content."),
  );
  $simple_notify['node-title'] = array(
    'name' => t("Node title"),
    'description' => t("The title of the node user has subscribed to."),
  );
  $simple_notify['node-link'] = array(
    'name' => t("Node URL"),
    'description' => t("The url of the node user has subscribed to."),
  );
  $simple_notify['referenced-title'] = array(
    'name' => t("Referenced Node title"),
    'description' => t("The title of the node user has subscribed to."),
  );
  $simple_notify['reference-link'] = array(
    'name' => t("Reference URL"),
    'description' => t("The url of the reference of the node user has subscribed to."),
  );

  return array(
    'types' => $types,
    'tokens' => array(
      'simple_notify' => $simple_notify,
    ),
  );
}

/**
 * Implements hook_tokens().
 */
function simple_notify_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if ($type == 'simple_notify') {
    // Loop through each of the available tokens.
    foreach ($tokens as $name => $original) {
      // Find the desired token by name.
      switch ($name) {
        case 'author-name':
          $replacements[$original] = $data['name'];
          break;

        case 'user-id':
          $replacements[$original] = $data['uid'];
          break;

        case 'node-title':
          $replacements[$original] = $data['node_title'];
          break;

        case 'node-link':
          $replacements[$original] = $data['node_link'];
          break;

        case 'referenced-title':
          $replacements[$original] = $data['referencednode_title'];
          break;

        case 'reference-link':
          $replacements[$original] = $data['reference_link'];
          break;
      }
    }
  }
  return $replacements;
}
