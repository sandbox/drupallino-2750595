<?php

/**
 * @file
 * Main file for simple_notify module.
 */

/**
 * Build the maill-loop to send notifications.
 *
 * @param object $data
 *    Maildata.
 */
function simple_notify_build_mail($data) {
  $account = user_load($data['user']);

  // Subject.
  $raw_subject = nl2br(variable_get('simple_notify_mail_subject', NULL));

  $subject = simple_notify_generate_mail_header(token_replace($raw_subject));

  // Body-Head.
  $raw_body_head = nl2br(token_replace(variable_get('simple_notify_mail_body_head', NULL), (array) $account));

  // Body-Foot.
  $raw_body_foot = nl2br(token_replace(variable_get('simple_notify_mail_body_foot', NULL)));

  $message = '<html><head><title>' . $raw_subject . '</title></head>
  <body>' . $raw_body_head . $data['loop'] . $raw_body_foot . '</body>
  </html>';

  // To send HTML mail, the Content-type header must be set.
  $headers = 'MIME-Version: 1.0' . "\r\n" . "Content-type: text/html;  charset=UTF-8" . "\r\n";

  // Use the sitewide mail.
  $headers .= 'From: ' . variable_get('simple_notify_mail_sender', NULL) . "\r\n";

  mail($account->mail, $subject, $message, $headers, '-f' . variable_get('simple_notify_mail_sender', NULL));
}

/**
 * Generate the Mailheader.
 *
 * @param string $content
 *    The content.
 * @param string $charset
 *    The used charset.
 *
 * @return string
 *    The encoded content.
 */
function simple_notify_generate_mail_header($content, $charset = 'UTF-8') {
  $encoded_content = sprintf(
    '=?%s?B?%s?=',
    strtoupper($charset),
    base64_encode($content)
  );
  return ($encoded_content);
}

/**
 * Build the maillop.
 *
 * @param object $user
 *    The subscription by user.
 *
 * @return array
 *    The looped subscriptions.
 */
function simple_notify_build_mailloop($user) {
  global $base_url;
  $loop = '';

  foreach ($user as $event) {

    if ($event['type'] == 'comment') {

      $message_part = simple_notify_get_comment($event['cid']);
      $comment_link = url('comment/' . $event['cid'], array(
        'fragment' => 'comment-' . $event['cid'],
        'absolute' => TRUE,
      ));

      $node = node_load($event['nid']);
      $node_link = url(drupal_lookup_path('alias', "node/" . $node->nid),
        array('absolute' => TRUE));

      $comment_loop = nl2br(variable_get('simple_notify_mail_loop_comments', ''));
      $replacements_subject = array(
        '@comment_subject' => $message_part['subject'],
        '@comment_author' => $message_part['name'],
        '@comment_link' => $comment_link,
        '@content_title' => $node->title,
        '@content_link' => $node_link,
      );

      $loop .= t($comment_loop, $replacements_subject);

    }

    if ($event['type'] == 'reference') {
      $node = node_load($event['nid']);
      $referenced_node = node_load($event['cid']);
      $referencednode_author = user_load($referenced_node->uid);

      $data = array(
        'node_title' => $node->title,
        'referencednode_title' => $referenced_node->title,
        'reference_link' => l(t('Referenced Link'), url(drupal_get_path_alias($base_url . '/node/' . $event['cid']))),
        'node_link' => l(t('Node Link'), url(drupal_get_path_alias($base_url . '/node/' . $node->nid))),
        'referencednode_author' => $referencednode_author,
      );

      $raw_body_reference_loop = nl2br(token_replace(variable_get('simple_notify_mail_loop_references', NULL), $data));

      $loop .= $raw_body_reference_loop;

    }
    simple_notify_set_sended($event['uid'], $event['cid']);
  }

  $data['loop'] = $loop;
  $data['user'] = $event['uid'];
  return $data;

}
