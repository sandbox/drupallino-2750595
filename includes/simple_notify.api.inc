<?php
/**
 * @file
 * API for simple notifier.
 */

/**
 * Find all references that added on content and write the notification.
 *
 * @param object $node
 *    Drupals node-object.
 */
function simple_notify_find_new_references($node) {
  global $user;

  $field_type = variable_get('simple_notify_' . $node->type, '');

  $notification = new stdClass();
  $notification->uid = $user->uid;
  $notification->nid = $node->nid;

  foreach ($field_type['entityreference'] as $key => $value) {
    $old_ids = array();
    $new_ids = array();
    foreach ($node->original->{$value}[LANGUAGE_NONE] as $item) {
      $old_ids[] = $item['target_id'];
    }
    foreach ($node->{$value}[LANGUAGE_NONE] as $item) {
      $new_ids[] = $item['target_id'];
    }
    foreach ($new_ids as $new) {
      if (!in_array($new, $old_ids)) {
        $notification->type = 'reference';
        simple_notify_write_single_notification($notification, $new);
      }
    }
  }
}

/**
 * Get the state of a subscriber.
 *
 * @return mixed
 *    Databaseresult.
 */
function simple_notify_get_state() {
  global $user;

  $result = db_select('simple_notify_subscriber', 'sns')
    ->fields('sns', array('value'))
    ->condition('uid', $user->uid, '=')
    ->execute()
    ->fetch();

  return $result;
}

/**
 * Write a single notification on event by content.
 *
 * @param object $data
 *    Notification object.
 * @param int $id
 *    Comment id.
 */
function simple_notify_write_single_notification($data, $id) {
  $result = db_select('simple_notify_single_subscriber', 'snss')
    ->fields('snss', array('value', 'nid', 'uid'))
    ->condition('nid', $data->nid, '=')
    ->execute();

  foreach ($result as $row) {
    db_insert('simple_notify_single_notify')
      ->fields(array(
        'uid' => $data->uid,
        'nid' => $data->nid,
        'cid' => $id,
        'type' => $data->type,
        'sended' => 0,
      ))
      ->execute();
  }
}

/**
 * Get all subscriptions are unsended. Only used on register notification.
 *
 * @return DatabaseStatementInterface|null
 *    Databasestatement.
 */
function simple_notify_get_subscriptons() {

  $query = db_select('simple_notify_subscriber', 'sns');
  $query->fields('sns', array('uid', 'value'));
  $query->condition('sns.value', 1, '=');

  $query->join('simple_notify_notify', 'snn', 'snn.uid = sns.uid');
  $query->fields('snn', array('nid', 'uid', 'cid'));

  $query->condition('snn.sended', 0, '=');
  $result = $query->execute();

  return $result;
}

/**
 * Get comments by cid.
 *
 * @param int $cid
 *    The comments id.
 *
 * @return mixed
 *    Databaseresult.
 */
function simple_notify_get_comment($cid) {
  $result = db_select('comment', 'c')
    ->fields('c', array('name', 'subject'))
    ->condition('c.cid', $cid, '=')
    ->execute()
    ->fetchAssoc();

  return $result;
}

/**
 * Get referenced nodes.
 *
 * @param int $cid
 *    The mathing id.
 *
 * @return mixed
 *    Database result.
 */
function simple_notify_get_reference($cid) {
  $result = db_select('node', 'n')
    ->fields('n', array('uid', 'title', 'nid'))
    ->condition('n.nid', $cid, '=')
    ->execute()
    ->fetchAssoc();

  return $result;
}

/**
 * Set subscriptions as sended.
 *
 * @param int $uid
 *    The users id.
 * @param int $cid
 *    The subscription id.
 */
function simple_notify_set_sended($uid, $cid) {
  db_update('simple_notify_notify')
    ->fields(array('sended' => 1))
    ->condition('uid', $uid)
    ->condition('cid', $cid)
    ->execute();

  db_update('simple_notify_single_notify')
    ->fields(array('sended' => 1))
    ->condition('uid', $uid)
    ->condition('cid', $cid)
    ->execute();
}

/**
 * Delete the single subscriptions.
 *
 * @param object $user
 *    The user object.
 * @param int $object
 *    The node id that is subscribed.
 */
function simple_notify_delete_single_subscription($user, $object) {
  // Delete the subscription by deactivating button.
  db_delete('simple_notify_single_subscriber')
    ->condition('nid', $object)
    ->condition('uid', $user->uid)
    ->execute();

  // Also delete all unsended notifications.
  db_delete('simple_notify_single_notify')
    ->condition('nid', $object)
    ->condition('uid', $user->uid)
    ->execute();
}

/**
 * Write the single subscriptions.
 *
 * @param object $user
 *    The users object.
 * @param int $object
 *   The node id that is subscribed.
 */
function simple_notify_write_single_subscription($user, $object) {
  $exists = db_query('SELECT uid FROM {simple_notify_single_subscriber} WHERE uid = :uid AND nid = :nid', array(
    ':uid' => $user->uid,
    ':nid' => $object,
  ))->fetchField();

  if ($exists) {
    db_update('simple_notify_single_subscriber')
      ->fields(array(
        'uid' => $user->uid,
        'nid' => $object,
        'value' => 1,
      ))
      ->condition('uid', $user->uid, '=')
      ->condition('nid', $object, '=')
      ->execute();
  }
  else {
    db_insert('simple_notify_single_subscriber')
      ->fields(array(
        'uid' => $user->uid,
        'nid' => $object,
        'value' => 1,
      ))
      ->execute();
  }
}

/**
 * Suspend user from notifications, we won't delete them for reactivation.
 *
 * @param object $account
 *    The users account object.
 */
function simple_notify_suspend_notification($account) {
  db_update('simple_notify_subscriber')
    ->fields(array(
      'value' => 0,
    ))
    ->condition('uid', $account->uid, '=')
    ->execute();
}

/**
 * Set the notification to the user. Used by Subscription on register.
 *
 * @param int $state
 *    Status.
 * @param object $account
 *    Users account.
 */
function simple_notify_write_notification($state, $account) {
  $exists = db_query('SELECT uid FROM {simple_notify_subscriber} WHERE uid = :uid',
    array(':uid' => $account->uid))->fetchField();

  if ($exists) {
    db_update('simple_notify_subscriber')
      ->fields(array(
        'uid' => $account->uid,
        'value' => $state,
      ))
      ->condition('uid', $account->uid, '=')
      ->execute();
  }
  else {
    db_insert('simple_notify_subscriber')
      ->fields(array(
        'uid' => $account->uid,
        'value' => $state,
      ))
      ->execute();
  }

}

/**
 * Get the subscriber by NID. Use on register notification.
 *
 * @param int $nid
 *    The nodeID.
 *
 * @return DatabaseStatementInterface|null
 *    The DatabaseQuery.
 */
function simple_notify_get_subscriber($nid) {
  $query = db_select('comment', 'c');
  $query->fields('c', array('subject', 'uid'));
  $query->condition('c.nid', $nid, '=');
  $query->join('simple_notify_subscriber', 'sns', 'c.uid = sns.uid');
  $query->fields('sns', array('value'));
  $query->condition('sns.value', 1, '=');
  $query->groupBy('uid');

  $result = $query->execute();

  return $result;
}

/**
 * Write the subscriptions. Use on register notification.
 *
 * @param array $subscribtions
 *    Array of subscriptions.
 * @param object $object
 *    Object with subscription infos.
 */
function simple_notify_write_subscription($subscribtions, $object, $type) {
  foreach ($subscribtions as $subscriber) {
    // Prevent to write a notification on own comments.
    if ($object->uid == $subscriber->uid) {
      continue;
    }
    // UID must be set.
    if ($subscriber->uid == '' || !isset($subscriber->uid) || $subscriber->uid == 0) {
      global $user;

      if (!is_object($subscriber)) {
        $subscriber = new stdClass();
      }
      $subscriber->uid = $user->uid;
    }

    db_insert('simple_notify_notify')
      ->fields(array(
        'uid' => $subscriber->uid,
        'nid' => $object->nid,
        'cid' => $object->cid,
        'type' => $type,
      ))
      ->execute();
  }
}

/**
 * Get single state of subscription.
 *
 * @param object $user
 *    The users object.
 * @param int $nid
 *    The node id of the subscription.
 *
 * @return int
 *   The state.
 */
function simple_notify_get_single_subscription_state($user, $nid) {
  $result = db_select('simple_notify_single_subscriber', 'snss')
    ->fields('snss', array('value'))
    ->condition('uid', $user->uid, '=')
    ->condition('nid', $nid, '=')
    ->execute()
    ->fetch();

  if (isset($result->value)) {
    return $result->value;
  }
  return 0;
}

/**
 * Get the single subscriber subscription on node ID.
 *
 * @return \DatabaseStatementInterface|null
 *    Database result.
 */
function simple_notify_get_single_subscriptions() {
  // Join with the notifications to the node ID that are not sended.
  $query = db_select('simple_notify_single_notify', 'snsn');
  $query->fields('snsn', array('nid', 'uid', 'cid', 'type'));
  $query->condition('snsn.sended', 0, '=');

  $result = $query->execute();

  return $result;
}

/**
 * Render our Notify-Links.
 *
 * @return string $output
 *   The links.
 */
function simple_notify_render_link() {

  drupal_add_library('system', 'drupal.ajax');

  global $user;
  $output = '';

  $nid = intval(filter_xss(arg(1)));
  $state = simple_notify_get_single_subscription_state($user, $nid);
  switch ($state) {
    case 1:
      $output .= "<div id='myDiv'>"
        . l(t('Unsubscribe'), 'simple_notify_ajax_link_callback/nojs/'
          . $nid . '/unsubscribe',
          array(
            'attributes' => array(
              'class' => array(
                'use-ajax',
                'on',
              ),
            ),
          )) . "</div>";
      break;

    default:
      $output .= "<div id='myDiv'>"
        . l(t('Subscribe'), 'simple_notify_ajax_link_callback/nojs/'
          . $nid . '/subscribe',
          array(
            'attributes' => array(
              'class' => array(
                'use-ajax',
                'off',
              ),
            ),
          )) . "</div>";

  }

  return $output;
}

/**
 * Ajax Link Response.
 *
 * @param int $argument
 *   Argument send by link.
 * @param string $type
 *   Type of response.
 *
 * @return null|string
 *    Return the response.
 */
function simple_notify_ajax_link_response($argument, $type = 'ajax') {
  if ($type == 'ajax') {

    global $user;

    switch (filter_xss(arg(3))) {
      case 'unsubscribe':
        simple_notify_delete_single_subscription($user, filter_xss(arg(2)));
        $output = "<div id='myDiv'>"
          . l(t('Subscribe'), 'simple_notify_ajax_link_callback/nojs/'
            . $argument . '/subscribe',
            array(
              'attributes' => array(
                'class' => array(
                  'use-ajax',
                  'off',
                  'button',
                ),
              ),
            )) . "</div>";;

        break;

      case 'subscribe':
        simple_notify_write_single_subscription($user, filter_xss(arg(2)));
        $output = "<div id='myDiv'>"
          . l(t('Unsubscribe'), 'simple_notify_ajax_link_callback/nojs/'
            . $argument . '/unsubscribe',
            array(
              'attributes' => array(
                'class' => array(
                  'use-ajax',
                  'on',
                  'button',
                ),
              ),
            )) . "</div>";;

        break;
    }

    if (isset($output)) {
      $commands[] = ajax_command_replace('#myDiv',
        '<div id="myDiv">' . $output . '</div>');
      $commands['effect'] = 'slide';
      $page = array('#type' => 'ajax', '#commands' => $commands);
      ajax_deliver($page);
    }
    else {
      watchdog(WATCHDOG_WARNING, 'Output of Notifier Button is not set.');
    }

  }
  else {
    $output = t("This is some content delivered via a page load.");
    return $output;
  }
}

/**
 * Implements template_preprocess_node().
 */
function simple_notify_preprocess_node(&$variables) {
  $content_types = variable_get('simple_notify_content_types', '');
  if ($variables['type'] === $content_types[$variables['type']] && $variables['view_mode'] === 'full') {
    $variables['notifier_button'] = simple_notify_render_link();
  }
}
